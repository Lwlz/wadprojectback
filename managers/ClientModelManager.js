var ClientModel = require('../models/ClientModel.js')
const mongoose = require('mongoose')

class ClientModelManager {
    constructor(){}

    _init(mongoClient){
        console.log("Initializing - ClientModelManager")
        this.entityManager = mongoClient.getInstance().model(ClientModel.getModelName(), ClientModel.getJSONObject())
        console.log('ClientModelManager connected: ' + this._isConnected())
    }

    getEntityManager(){
        return this.entityManager
    }

    _isConnected(){
        return this.entityManager != undefined    
    }

    getAllClients(){
        return this.getEntityManager()
                    .find({})
    }
    createClient(clientModel){

        if(!(clientModel instanceof ClientModel))
            return;

        return new this.entityManager(clientModel.toJSON()).save()
    }

    getByName(name){
        return this.getEntityManager()
                   .find({name: name})
    }
}

module.exports = new ClientModelManager()