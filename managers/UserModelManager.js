var UserModel = require('../models/UserModel.js')
const mongoose = require('mongoose')

class UserModelManager {
    constructor(){}

    _init(mongoClient){
        console.log("Initializing - UserModelManager")
        this.entityManager = mongoClient.getInstance().model(UserModel.getModelName(), UserModel.getJSONObject())
        console.log('UserModelManager connected: ' + this._isConnected())
    }

    getEntityManager(){
        return this.entityManager
    }

    _isConnected(){
        return this.entityManager != undefined    
    }

    createUser(userModel){

        if(!(userModel instanceof UserModel))
            return;

        return new this.entityManager(userModel.toJSON()).save()
    }

    getAllUsers(){
        return this.getEntityManager()
                   .find({})
    }

    getUserByName(name){
        return this.getEntityManager()
                   .find({name: name})
    }

    getUserByNameAndLastName(name, lastName){
        return this.getEntityManager()
                   .find({name: name, lastName: lastName})
    }

    getUserByNameAndPassword(name, password){
        return this.getEntityManager()
                   .find({name: name, password: password})
    }

    findOne(json){
        return this.getEntityManager().find(json)
    }
}

module.exports = new UserModelManager()