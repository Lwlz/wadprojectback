var SaleModel = require('../models/SaleModel.js')
const mongoose = require('mongoose')

class SaleModelManager {
    constructor(){}

    _init(mongoClient){
        console.log("Initializing - SaleModelManager")
        this.entityManager = mongoClient.getInstance().model(SaleModel.getModelName(), SaleModel.getJSONObject())
        console.log('SaleModelManager connected: ' + this._isConnected())
    }

    getEntityManager(){
        return this.entityManager
    }

    _isConnected(){
        return this.entityManager != undefined    
    }
    
     getSalesByUserId(userId){
        return this.getEntityManager()
                    .find({userId: userId})

    }

    createSale(saleModel){
        
        if(!(saleModel instanceof SaleModel))
            return;

        return new this.entityManager(saleModel.toJSON()).save()
    }
}

module.exports = new SaleModelManager()