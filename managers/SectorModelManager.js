var SectorModel = require('../models/SectorModel.js')
const mongoose = require('mongoose')

class SectorModelManager {
    constructor(){}

    _init(mongoClient){
        console.log("Initializing - SectorModelManager")
        this.entityManager = mongoClient.getInstance().model(SectorModel.getModelName(), SectorModel.getJSONObject())
        console.log('SectorModelManager connected: ' + this._isConnected())
    }

    getEntityManager(){
        return this.entityManager
    }

    _isConnected(){
        return this.entityManager != undefined    
    }
}

module.exports = new SectorModelManager()