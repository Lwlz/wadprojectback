var ProductModel = require('../models/ProductModel.js')
const mongoose = require('mongoose')

class ProductModelManager {
    constructor(){}

    _init(mongoClient){
        console.log("Initializing - ProductModelManager")
        this.entityManager = mongoClient.getInstance().model(ProductModel.getModelName(), ProductModel.getJSONObject())
        console.log('ProductModelManager connected: ' + this._isConnected())
    }

    getEntityManager(){
        return this.entityManager
    }

    _isConnected(){
        return this.entityManager != undefined    
    }

    getByName(name){
        return this.getEntityManager()
                   .find({name: name})
    }

    getProductBySaleId(id){
        return this.getEntityManager()
                   .find({saleId: id})
    }

    createProduct(productModel){

        if(!(productModel instanceof ProductModel))
            return;

        return new this.entityManager(productModel.toJSON()).save()
    }
}

module.exports = new ProductModelManager()