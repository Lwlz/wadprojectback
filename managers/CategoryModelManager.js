var CategoryModel = require('../models/CategoryModel.js')
const mongoose = require('mongoose')

class CategoryModelManager {
    constructor(){}

    _init(mongoClient){
        console.log("Initializing - CategoryModelManager")
        this.entityManager = mongoClient.getInstance().model(CategoryModel.getModelName(), CategoryModel.getJSONObject())
        console.log('CategoryModelManager connected: ' + this._isConnected())
    }

    getEntityManager(){
        return this.entityManager
    }

    _isConnected(){
        return this.entityManager != undefined    
    }
}

module.exports = new CategoryModelManager()