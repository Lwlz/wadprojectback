class TokenUserMap {
    constructor(){}

    _init(){
        this.tokenMap = new Map();
    }

    addToken(username){
        var nToken = Buffer.from(new Date().getTime.toString).toString('base64')
        console.log("Added Token for " + username + " -> " + nToken)
        this.tokenMap.set(username, nToken);
    }

    hasToken(username){
        return this.tokenMap.has(username);
    }

    deleteToken(username){
        this.tokenMap.delete(username)
    }
}

module.exports = new TokenUserMap()