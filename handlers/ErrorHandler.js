exports.throw500Error = function(response, message){
    response.status = 500;
    response.message = message;
}
exports.throw400Error = function(response, message){
    response.status = 400;
    response.send(message);
}
exports.throw404Error = function(response, message){
    response.status = 404;
    response.send(message);
}