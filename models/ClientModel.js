class ClientModel {

    constructor(parameters){
        this.clientName  = parameters.clientName,
        this.clientSiret = parameters.clientSiret,
        this.phone       = parameters.phone

        this.toJSON = function(){
            return {
                clientName: this.clientName,
                clientSiret: this.clientSiret,
                phone: this.phone
            }
        }
    } 
    
    static getModelName(){
        return "client"
    }
    static getJSONObject(){
        return  {
                    clientName   : {type: String, required: true},
                    clientSiret  : {type: String, required: true},
                    phone        : {type: String, required: true},
                }
    }  
}

module.exports = ClientModel