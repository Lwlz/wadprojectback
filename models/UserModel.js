class UserModel {

    constructor(parameters){
        this.lastName   = parameters.lastName
        this.name       = parameters.name
        this.mail       = parameters.mail
        this.phone      = parameters.phone
        this.society    = parameters.society
        this.siret      = parameters.siret
        this.newsletter = parameters.newsletter
        this.created    = parameters.created
        this.isActive   = parameters.isActive
        this.password   = parameters.password

        this.toJSON = function(){
            return {
                lastName: this.lastName,
                name: this.name,
                mail: this.mail,
                phone: this.phone,
                society: this.society,
                siret: this.siret,
                newsletter: this.newsletter,
                created: this.created,
                isActive: this.isActive,
                password: this.password
            }
        }
    } 
    
    static getModelName(){
        return "user"
    }
    static getJSONObject(){
        return  {
                    lastName    : {type: String, required: true},
                    name        : {type: String, required: true},
                    society     : {type: String, required: true},
                    siret       : {type: String, required: true},
                    mail        : {type: String, required: true},
                    phone       : {type: String, required: true},
                    created     : {type: Date, default: new Date()},
                    newsletter  : {type: Boolean, default: true},
                    isActive    : {type: Boolean, default: true},
                    password    : {type: String, required: true}
                }
    }  
}

module.exports = UserModel
