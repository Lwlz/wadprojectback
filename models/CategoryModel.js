class CategoryModel {

    constructor(parameters){
        this.name = parameters.name

        this.toJSON = function(){
            return {
                name: this.name
            }
        }
    } 
    
    static getModelName(){
        return "category"
    }
    static getJSONObject(){
        return  {
                    name   : {type: String, required: true}
                }
    }  
}

module.exports = CategoryModel