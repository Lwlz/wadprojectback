class ProductModel {

    constructor(parameters){
        this.name        = parameters.productName,
        this.provider    = parameters.provider,
        this.categoryId  = parameters.categoryId,
        this.sectorId    = parameters.sectorId

        this.toJSON = function(){
            return {
                name: this.name,
                provider: this.provider,
                categoryId: this.categoryId,
                sectorId: this.sectorId
            }
        }
    } 
    
    static getModelName(){
        return "product"
    }
    static getJSONObject(){
        return  {
                    name       : {type: String, required: true},
                    provider   : {type: String, required: true},
                    categoryId : {type: String, required: true},
                    sectorId   : {type: String, required: true},
                }
    }  
}

module.exports = ProductModel