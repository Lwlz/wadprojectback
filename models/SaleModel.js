class SaleModel {

    constructor(parameters){
        this.userId      = parameters.userId,
        this.productId   = parameters.productId,
        this.clientId    = parameters.clientId,
        this.createdDate = parameters.createdDate,
        this.quantity    = parameters.quantity,
        this.tva         = parameters.tva,
        this.ht          = parameters.ht

        this.toJSON = function(){
            return {
                userId: this.userId,
                productId: this.productId,
                clientId: this.clientId,
                createdDate: this.createdDate,
                quantity: this.quantity,
                tva: this.tva,
                ht: this.ht
            }
        }
    } 
    
    static getModelName(){
        return "sale"
    }
    static getJSONObject(){
        return  {
                    userId      : {type: String, required: true},
                    productId   : {type: String, required: true},
                    clientId    : {type: String, required: true},
                    createdDate : {type: Date, default: new Date()},
                    quantity    : {type: Number, required: true},
                    
                    tva         : {type: Number, default: 20},
                    ht          : {type: Number, required: true}
                }
    }  
}

module.exports = SaleModel