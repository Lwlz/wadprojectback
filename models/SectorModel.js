class SectorModel {

    constructor(parameters){
        this.name = parameters.name

        this.toJSON = function(){
            return {
                name: this.name
            }
        }
    } 
    
    static getModelName(){
        return "sector"
    }
    static getJSONObject(){
        return  {
                    name   : {type: String, required: true}
                }
    }  
}

module.exports = SectorModel