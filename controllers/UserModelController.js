var UserModel        = require("../models/UserModel")
var ErrorHandler     = require('../handlers/ErrorHandler')
var UserModelManager = require('../managers/UserModelManager')

const password_salt = Buffer.from("ynov_salt_:)").toString('base64')

function isValidUserInput(parameters){
    return parameters.lastName != undefined &&
           parameters.name != undefined &&
           parameters.mail != undefined &&
           parameters.phone != undefined &&
           parameters.society != undefined &&
           parameters.siret != undefined &&
           parameters.password != undefined
}

exports.generatePassword = function(password){
    return password_salt + Buffer.from(password).toString('base64')
}

exports.get_all_users = function(req, res){

    UserModelManager.getAllUsers().exec(function(err, users){
        res.send(users)
    })
}
exports.get_user_by_name = function(req, res){

    UserModelManager.getUserByName(req.query.name).exec(function(err, user){
        if(user == undefined || user.length == 0){
            ErrorHandler.throw404Error(res, 'Unfound user')
        }
        else {
            res.send(user)
        }
    })
}

exports.create_user = function(req, res){

    if(!isValidUserInput(req.query)){
        ErrorHandler.throw400Error(res, "Bad input, please provide all field")
        return
    }

    // CHECK that the user already exist
    UserModelManager.getUserByName(req.query.name).exec(function(err, user){

        if(user != undefined && user.length > 0){
            ErrorHandler.throw400Error(res, 'This user already exist');
            return;
        }

        var nUser = new UserModel(req.query);

        // COMPUTE salted password
        nUser.password = password_salt + Buffer.from(req.query.password).toString('base64')

        res.send(UserModelManager.createUser(nUser));
    });

}
