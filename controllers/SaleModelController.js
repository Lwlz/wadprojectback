var ErrorHandler        = require('../handlers/ErrorHandler')
var SaleModelManager    = require('../managers/SaleModelManager')
var DashBoardDto        = require('../DTO/DashBoardDto')
var ClientModelManager  = require('../managers/ClientModelManager')
var ProductModelManager = require('../managers/ProductModelManager')
var CategoryModelManager = require('../managers/CategoryModelManager')
var SectorModelManager   = require('../managers/SectorModelManager')

exports.get_sales_by_userId = function(req, res){

    console.log(req.query.userId)

    SaleModelManager.getSalesByUserId(req.query.userId).exec(function(err, sales){

        if(err) {
            ErrorHandler.throw500Error(res, "Internal Error while loading sales") 
            return;
        }

        var saleList = [];

        sales.forEach(sale => {

            console.log(sale)

            var nDTO = new DashBoardDto.SalesDashBoardDtoGet();
            nDTO.userId  = req.query.userId;
            nDTO.tva     = sale.tva;
            nDTO.ht      = sale.ht;
            nDTO.quantity = sale.quantity;
            nDTO.createdDate = sale.createdDate;
            nDTO.client = sale.clientId;
            nDTO.product = sale.productId;

            // LOAD Product attached to the current SaleModel
            ProductModelManager.getEntityManager().find({id: sale.productId}).exec(function(err, product){

                console.log(product);
                

                nDTO.productName = product.name;

                // LOAD Category
                CategoryModelManager.getEntityManager().find({id: product.categoryId}).exec(function(err, category){

                    console.log(category);

                    nDTO.category = category.name;
                });
                // LOAD Sector
                SectorModelManager.getEntityManager().find({id: product.sectorId}).exec(function(err, sector){

                    console.log(sector);

                    nDTO.sector = sector.name;
                });
            });

            // LOAD Client attached to the current SaleModel
            ClientModelManager.getEntityManager().find({id: sale.clientId}).exec(function(err, client){
                nDTO.client = client.clientName;
            });

            saleList.push(nDTO)
        });

        res.send(saleList)
    })
}