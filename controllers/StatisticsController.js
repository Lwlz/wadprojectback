var ErrorHandler        = require('../handlers/ErrorHandler')
var SaleModelManager    = require('../managers/SaleModelManager')
var DashBoardDto        = require('../DTO/DashBoardDto')
var ProductModelManager = require('../managers/ProductModelManager')
var CategoryModelManager = require('../managers/CategoryModelManager')
var SectorModelManager   = require('../managers/SectorModelManager')

exports.sales_statistics_last_month = function(req, res){

    SaleModelManager.getSalesByUserId(req.query.userId).exec(function(err, sales){

        if(err){
            ErrorHandler.throw400Error(res, "Error while loading Sales for the current User");
            return;
        }

        var statisticList = [];

        sales.forEach(sale => {

            var nDTO = new DashBoardDto.SalesStatisticsDtoGet();

            var statArray = statisticList.filter(stat => stat.productId = sale.productId);

            if(statArray.length > 0){
                var index = statisticList.indexOf(statArray[0]);

                statisticList[index].quantity += sale.quantity;
            }
            else {

                // LOAD Product attached to the current SaleModel
                ProductModelManager.getEntityManager.find({id: sale.productId}).exec(function(err, product){
                
                    nDTO.productId   = product.id;
                    nDTO.productName = product.name;

                    // LOAD Category
                    CategoryModelManager.getEntityManager.find({id: product.categoryId}).exec(function(err, category){
                        nDTO.category = category.name;
                    });
                    // LOAD Sector
                    SectorModelManager.getEntityManager.find({id: product.sectorId}).exec(function(err, sector){
                        nDTO.sector = sector.name;
                    });
                });

                // LOAD Client attached to the current SaleModel
                ClientModelManager.getEntityManager.find({id: sale.clientId}).exec(function(err, client){
                    nDTO.clientName = client.clientName;
                });

                console.log(nDTO)

                statisticList.push(nDTO)
            }
        });

        console.log(statisticList)

        res.send(statisticList)

    });

}