const express  = require('express')
const app = express()
const port = 3000

const TokenUserMap = require('../handlers/TokenUserMap')

const SaleModel = require('../models/SaleModel')

const UserModelManager    = require('../managers/UserModelManager')
const SaleModelManager    = require('../managers/SaleModelManager')
const ClientModelManager  = require('../managers/ClientModelManager')
const ProductModelManager = require('../managers/ProductModelManager')
const CategoryModelManager = require('../managers/CategoryModelManager')
const SectorModelManager    = require('../managers/SectorModelManager')

const UserModelController = require('../controllers/UserModelController')
const LoginController = require('../controllers/LoginController')
const ClientModelController = require('../controllers/ClientModelController')
const SaleModelController = require('../controllers/SaleModelController')
const StatisticsController = require('../controllers/StatisticsController')
const ProductModelController = require('../controllers/ProductModelController')

const modelManagerArray = [UserModelManager, SaleModelManager, ClientModelManager, ProductModelManager, CategoryModelManager, SectorModelManager];

var MongoClient = require('../mongodb/MongoClient.js')

// CONNECT to MongoDB
MongoClient._connect().then(() => {

    modelManagerArray.forEach(function(manager){ manager._init(MongoClient) })

    TokenUserMap._init()

    // Server - Enable Listener once the DataBase is accessible
    app.listen(port, (err) => {
        if (err) return console.log('ERROR Occured - CANCELED', err);
        console.log(`3: -------- NodeJS Server - Listening on ${port} --------`)
    })
})

app.use(function(req, res, next){

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // PROTO-AUTH Code
    if(req.headers 
        && req.headers.cookie 
            && req.headers.cookie.split(' ').filter(ck => ck.startsWith('JWT')).length > 0) {
                req.user = true;
    } else {
        req.user = undefined
    }

    // Prevent from duplicate login
    if(req.url.startsWith("/auth/login")){
        
        if(req.user){
            res.send("Already Connected");
        }
        else {
            next();
        }
        return;
    }

    // Check for login
    if(LoginController.login_required(req, res)){
        console.log("OK")
        next();
    }
    else {
        console.log("PAS OK")
        res.status(401).json({ message: 'Unauthorized user!' }).send()
    }
})

app.post('/auth/login', LoginController.sign_in);
app.post('/auth.logout', LoginController.logout);
app.post('/auth/signup', LoginController.sign_up);

app.get ('/rest/User', UserModelController.get_all_users);
app.get ('/rest/User:name', UserModelController.get_user_by_name);
app.post('/rest/User', UserModelController.create_user);

app.get ('/rest/Client', ClientModelController.get_all_clients);
app.post('/rest/Client', ClientModelController.create_client);
app.get ('/rest/Client:userId', ClientModelController.get_clients_of);

app.get('/rest/Sales', SaleModelController.get_sales_by_userId);

// PROTO-EndPoint
app.get('/rest/Sales/create', function(req, res){
    SaleModelManager.createSale(new SaleModel(req.query));
    res.send(req.query);
});

app.get('/rest/Statistics/Sales:userId', StatisticsController.sales_statistics_last_month);

app.post('/rest/Product', ProductModelController.create_product);