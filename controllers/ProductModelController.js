var ProductModel = require('../models/ProductModel')
var SaleModelManager    = require('../managers/SaleModelManager')
var ProductModelManager = require('../managers/ProductModelManager')
var CategoryModelManager = require('../managers/CategoryModelManager')
var SectorModelManager   = require('../managers/SectorModelManager')

exports.create_product = function(req, res){

    ProductModelManager.getByName().exec(function(err, product){

        if(err){
            ErrorHandler.throw400Error(res, "Error while loading Products")
            return;
        }

        if(product != undefined && product.lenght > 0){
            ErrorHandler.throw400Error(res, "Already exiting product");
            return;
        }
        console.log(req.query)
        ProductModelManager.createProduct(new ProductModel(req.query));

        res.send(req.query);
    })

}