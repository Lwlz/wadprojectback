var SectorModel = require('../models/ProductModel')
var SectorModelManager   = require('../managers/SectorModelManager')

exports.create_sector = function(req, res){

    SectorModelManager.getByName().exec(function(err, sector){

        if(err){
            ErrorHandler.throw400Error(res, "Error while loading Sector")
            return;
        }

        if(sector != undefined && sector.lenght > 0){
            ErrorHandler.throw400Error(res, "Already exiting sector");
            return;
        }
        console.log(req.query)
        SectorModelManager.createSector(new SectorModel(req.query));

        res.send(req.query);
    })

}