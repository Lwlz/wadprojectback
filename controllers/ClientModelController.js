var ClientModel         = require("../models/ClientModel")
var ErrorHandler        = require('../handlers/ErrorHandler')
var ClientModelManager  = require('../managers/ClientModelManager')
var ClientDto           = require('../DTO/ClientDto')
var SaleModelManager    = require('../managers/SaleModelManager')

exports.create_client = function(req, res){

    ClientModelManager.getEntityManager().find({clientName: req.query.clientName}).exec(function(err, client){
        if(err){
            ErrorHandler.throw400Error(res, "Error while loading Clients")
            return;
        }

        if(client != undefined && client.lenght > 0){
            ErrorHandler.throw400Error(res, "Already exiting client");
            return;
        }
        console.log(req.query)
        ClientModelManager.createClient(new ClientModel(req.query));

        res.send(req.query);
    });
}

exports.get_all_clients = function(req, res){

    ClientModelManager.getAllClients().exec(function(err, clients){

        var clientList = [];

        clients.forEach(client => {
            clientList.push(new ClientDto.ClientDtoGet(client.clientName, client.clientSiret, client.phone))
        });

        console.log(clientList)

        res.send(clientList)
    })
}

exports.get_clients_of = function(req, res) {

    // LOAD Each Client that have a Sale for this user
    SaleModelManager.getEntityManager.find({userId: req.query.userId}).exec(function(err, sales){

        if(err){
            ErrorHandler.throw400Error(res, "Error while loading Sales for this user");
            return;
        }

        var clientDTOArray = [];

        sales.forEach(sale => {

            // LOAD Client of the current sale
            ClientModelManager.getEntityManager.find({id: sale.clientId}).exec(function(err, client){
                clientDTOArray.push(new ClientDto.ClientDtoGet(client.clientName, client.clientSiret, client.phone))
            });

        })

        res.send(clientDTOArray)
    });

}