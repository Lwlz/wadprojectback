var TokenUserMap = require('../handlers/TokenUserMap')
var UserModelManager = require('../managers/UserModelManager')
var UserModelController = require('../controllers/UserModelController')

var jwt = require('jsonwebtoken')

exports.login_required = function(req, res) {
    return req.user != undefined;
  };

exports.sign_up = function(req, res){

    // simply use the same function as the User Creation
    // to be improved for more behaviours
    UserModelController.create_user(req, res)

}

exports.logout = function(req, res){
    req.clearCookie('JWT');
    req.clearCookie('email');

    res.clearCookie('JWT');
    res.clearCookie('email');

    res.send(200, "Logout successfully");
}

exports.sign_in = function(req, res){

    console.log("SIGNING_IN")

    UserModelManager.getEntityManager().find({email: req.query.email}).exec(function(err, user) {

        console.log(user)

        if (err) throw err;
        if (!user) {
            console.log("!user")
            res.status(401).json({ message: 'Authentication failed. User not found.' });
        } else if (user) {
            if (!user.password == UserModelController.generatePassword(req.query.password)) {
                res.status(401).json({ message: 'Authentication failed. Wrong password.' });
            } else {
                var token = jwt.sign({ email: user.email, fullName: user.fullName, _id: user._id}, 'RESTFULAPIs');
                
                res.cookie('JWT', token, { maxAge: 900000, httpOnly: true });
                res.cookie('email', user.email, { maxAge: 900000, httpOnly: true });

                return res.json({token: token});
            }
        }

        res.send();
    });
}