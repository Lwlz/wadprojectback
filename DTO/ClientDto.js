class ClientDtoGet {
    constructor(name, siret, phone){
        this.clientName  = name,
        this.clientSiret = siret,
        this.phone       = phone
    }
}

module.exports = {
    ClientDtoGet
}