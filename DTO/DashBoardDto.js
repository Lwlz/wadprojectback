class SalesDashBoardDtoGet {
    constructor(){
        this.userId        = undefined,
        this.productName   = undefined,
        this.clientName    = undefined,
        this.createdDate   = undefined,
        this.quantity      = undefined,
        this.category      = undefined,
        this.sector        = undefined,
        this.tva           = undefined,
        this.ht            = undefined
    }
}

class SalesStatisticsDtoGet {
    constructor(){
        this.productId   = undefined,
        this.productName = undefined,
        this.quantity    = undefined,
        this.client      = undefined,
        this.category    = undefined,
        this.sector      = undefined,
        this.ht          = undefined
    }
}

module.exports = {
    SalesDashBoardDtoGet,
    SalesStatisticsDtoGet
}