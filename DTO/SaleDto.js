class SaleDtoGet {
    constructor(parameters){
        this.userId        = parameters.userId,
        this.productName   = parameters.productId,
        this.clientName    = parameters.clientId,
        this.createdDate   = parameters.createdDate,
        this.quantity      = parameters.quantity,
        this.tva           = parameters.tva,
        this.ht            = parameters.ht
    }
}

module.exports = {
    SaleDtoGet
}